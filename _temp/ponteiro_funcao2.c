#include <stdio.h>
#include <stdlib.h>



int incrementa(int n){
    return n + 1;
}

int decrementa(int n){
    return n - 1;
}

void map(int* v, int tam, int (*funcao)(int)){
    for (int i=0; i < tam; i++){
        v[i] = funcao(v[i]);
    }
}

int* map2(int* v, int tam, int (*funcao)(int)){
    int* novo = (int*) calloc(tam, sizeof(int));

    for (int i=0; i < tam; i++){
        novo[i] = funcao(v[i]);
    }
    return novo;
}

void imprimeVetor(int* v, int tam){
    printf("[");
    for (int i=0; i < tam; i++){
        printf("%d", v[i]);
        if(i < tam-1) printf(",");
    }
    printf("]");
}


int main(){

    int vetor[5] = {1,2,3,4,5};

    int* copia = map2(vetor, 5, incrementa);
    imprimeVetor(copia,5);
    imprimeVetor(vetor,5);

    // map(vetor, 5, decrementa);


    return 0;
}