#include <stdio.h>

int soma(int a, int b){
    return a + b;
}

int subtrai(int a, int b){
    return a - b;
}
int multiplica(int a, int b){
    return a * b;
}
int divide(int a, int b){
    return a / b;
}


int operacao(int a, int b, int (*funcao)(int, int)){
    int resultado = funcao(a,b);
    return resultado;
}

int main(){

    int x = 10;
    int y = 20;

    int res = operacao(10, 20, subtrai);
    printf("%d\n", res);

    return 0;
}